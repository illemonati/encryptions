use crate::common::{EncryptDecryptor, EncryptDecryptorError};
use std::str;

pub struct XorRepeatingEncryptDecryptor;

impl EncryptDecryptor for XorRepeatingEncryptDecryptor {
    fn encrypt<'a>(msg: &'a [u8], key: &'a [u8]) -> Result<Vec<u8>, EncryptDecryptorError> {
        if key.len() < 1 {
            return Err(EncryptDecryptorError::new("Key length must be at least 1"));
        }
        let mut result = vec![0u8; msg.len()];
        for (i, m) in msg.iter().enumerate() {
            let k = key[i % key.len()];
            result[i] = m ^ k;
        }
        Ok(result)
    }
    fn decrypt<'a>(msg: &'a [u8], key: &'a [u8]) -> Result<Vec<u8>, EncryptDecryptorError> {
        Self::encrypt(msg, key)
    }

    fn encrypt_str<'a>(msg: &'a str, key: &'a str) -> Result<String, EncryptDecryptorError> {
        match Self::encrypt(msg.as_bytes(), key.as_bytes()) {
            Ok(m) => match str::from_utf8(&m) {
                Ok(s) => Ok(String::from(s)),
                Err(_) => Err(EncryptDecryptorError::new("UTF-8 error")),
            },
            Err(e) => Err(e),
        }
    }

    fn decrypt_str<'a>(msg: &'a str, key: &'a str) -> Result<String, EncryptDecryptorError> {
        Self::encrypt_str(msg, key)
    }
}
