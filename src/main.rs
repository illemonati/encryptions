use encryptions::{EncryptDecryptor, XorRepeatingEncryptDecryptor};

fn main() {
    let msg = "Hello World. I am the creator of geotoad 144444444444000000000";
    let key = "Geotoad";
    let xor_encrypted_msg = XorRepeatingEncryptDecryptor::encrypt_str(msg, key).unwrap();
    let xor_decrypted_msg =
        XorRepeatingEncryptDecryptor::decrypt_str(&xor_encrypted_msg, key).unwrap();
    println!(
        "Xored Msg:\n--------------------------------\n{}\n-------------------------------\n\n",
        &xor_encrypted_msg
    );
    println!(
        "Xor Decrypted Msg:\n--------------------------------\n{}\n-------------------------------",
        &xor_decrypted_msg
    );
}
