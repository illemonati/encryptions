use std::fmt;

#[derive(Debug, Clone)]
pub struct EncryptDecryptorError {
    reason: String,
}

impl EncryptDecryptorError {
    pub fn new(reason: &str) -> EncryptDecryptorError {
        EncryptDecryptorError {
            reason: String::from(reason),
        }
    }
}

impl fmt::Display for EncryptDecryptorError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", &self.reason)
    }
}

pub trait EncryptDecryptor {
    fn encrypt<'a>(msg: &'a [u8], key: &'a [u8]) -> Result<Vec<u8>, EncryptDecryptorError>;
    fn decrypt<'a>(msg: &'a [u8], key: &'a [u8]) -> Result<Vec<u8>, EncryptDecryptorError>;
    fn encrypt_str<'a>(msg: &'a str, key: &'a str) -> Result<String, EncryptDecryptorError>;
    fn decrypt_str<'a>(msg: &'a str, key: &'a str) -> Result<String, EncryptDecryptorError>;
}
